import React, {
  Component
} from 'react';
// import logo from './logo.svg';
// import './App.css';
import Child from './ChildComponent';

class Parent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0
    }
    this.incrementParentCount = this.incrementParentCount.bind(this);
  }

  incrementParentCount() {
    let updatedCount=this.state.count;
     updatedCount++;
    this.setState({
      count: updatedCount
    })
  }
  render() {
    let childElement = ( < Child incrementParentCount = {this.incrementParentCount}/>);
      return ( 
        <div className = "ParentCounter" > 
        {childElement} 
        <h3> count: {this.state.count} </h3> 
        </div>
      );
    }
  }

  export default Parent;