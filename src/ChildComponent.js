import React, { Component } from 'react';
// import logo from './logo.svg';
// import './App.css';
//  import Parent from './ParentComponent';

class Child extends Component {

    constructor(props){
        super(props);
        this.doParentToggleFromChild = this.doParentToggleFromChild.bind(this)
}


  doParentToggleFromChild() {
     this.props.incrementParentCount(); 
}

  render() {
    
    return (
      <div className="ChildCounter">
      <button onClick={this.doParentToggleFromChild}>Click me to say how many times you have clicked</button>

      </div>
    );
  }
}

export default Child;